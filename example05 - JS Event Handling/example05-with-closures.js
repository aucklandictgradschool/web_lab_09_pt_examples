"use strict";

var nextButton = 1;

// This function will return an event handling function which will add a new button.
// The new button will name the button which was clicked which caused it to be added.
//
// This odd syntax is how you can pass a variable (in this case, "num"), that will be available
// to the event handler function (in this case, "onclick"). It is known as a closure.
// For more information, see this link: https://www.w3schools.com/js/js_function_closures.asp
// Or example 06.
function addButton_Closure(num) {
    return function(event) {

        var text = "This is button #" + nextButton + ". It was added by button #" + num + ".";
        var newButton = document.createElement("button");
        newButton.innerHTML = text;

        // Set event handler for new button
        newButton.onclick = addButton_Closure(nextButton);

        nextButton++;

        // Add the button
        var newP = document.createElement("p");
        newP.appendChild(newButton);
        // We can get the body directly using "document.body" - i.e. we don't have to use findElementsByTagName here.
        document.body.appendChild(newP);

    }
}

// This is the way, in JavaScript, of setting the "body onload" event handler (see examples 1 and 2).
window.onload = function(event) {

    alert("The page has been loaded!");

    // Get the text
    var p = document.getElementById("asimpleparagraph");
    p.onmouseover = function() {

        // "this", in an event handler funciton, will refer to the thing whose event you're handling.
        // In this case - the <p> #asimpleparagraph.
        if (!this.classList.contains("redText")) {
            this.classList.add("redText");
        }
    }

    // An alternative way of adding an event listener to an element.
    // NOTE: "mouseout", instead of "onmouseout".
    p.addEventListener("mouseout", function() {

        // "this", in an event handler funciton, will refer to the thing whose event you're handling.
        // In this case - the <p> #asimpleparagraph.
        this.classList.remove("redText");
    });

    // Get the first button
    var button = document.getElementsByTagName("button")[0];

    // Hook up event handler for first button
    button.onclick = addButton_Closure(0);

}