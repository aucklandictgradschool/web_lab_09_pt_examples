"use strict";

var nextButton = 1;

// This function will be called when a button is clicked. It will add a new button, which will itself also call this function when clicked.
// The new button will name the button which was clicked which caused it to be added.
function addButton() {

    // Getting the number of this button which was clicked.
    // "this" refers to "this button which was clicked".
    // The button's dataset is an object to which we can read / write arbitrary properties (see below).
    var myIndex = this.dataset.index;

    // Get the text to display in the new button we're about to create.
    var text = "This is button #" + nextButton + ". It was added by button #" + this.dataset.index + ".";

    // Create the new button.
    var newButton = document.createElement("button");
    newButton.innerHTML = text;

    // Set the new button's data
    newButton.dataset.index = nextButton;

    // Set event handler for new button
    newButton.onclick = addButton;

    nextButton++;

    // Add the button
    var newP = document.createElement("p");
    newP.appendChild(newButton);
    // We can get the body directly using "document.body" - i.e. we don't have to use findElementsByTagName here.
    document.body.appendChild(newP);
}

// This is the way, in JavaScript, of setting the "body onload" event handler (see examples 1 and 2).
window.onload = function(event) {

    alert("The page has been loaded!");

    // Get the text
    var p = document.getElementById("asimpleparagraph");
    p.onmouseover = function() {

        // "this", in an event handler funciton, will refer to the thing whose event you're handling.
        // In this case - the <p> #asimpleparagraph.
        if (!this.classList.contains("redText")) {
            this.classList.add("redText");
        }
    }

    // An alternative way of adding an event listener to an element.
    // NOTE: "mouseout", instead of "onmouseout".
    p.addEventListener("mouseout", function() {

        // "this", in an event handler funciton, will refer to the thing whose event you're handling.
        // In this case - the <p> #asimpleparagraph.
        this.classList.remove("redText");
    });

    // Get the first button
    var button = document.getElementsByTagName("button")[0];

    // An HTML element's "dataset" property is a JavaScript Object - to which we can add any extra properties we wish,
    // just like when we create our own JavaScript objects.
    // Here we're storing this "index" value. We'll use it later.
    button.dataset.index = 0;

    // Hook up event handler for first button. Remember that we can pass functions around as variables, by naming the function
    // but not writing any brackets () or supplying any arguments.
    // In VS code, note how this appears blue, as opposed to yellow if we actually call the function.
    button.onclick = addButton;

}